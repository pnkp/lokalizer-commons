export declare class RequestOptions {
    headers?: object;
    params?: object;
    data?: object;
    timeout?: number;
    url: string;
    method: RequestMethodEnum;
}
export declare enum RequestMethodEnum {
    GET = "GET",
    POST = "POST"
}
