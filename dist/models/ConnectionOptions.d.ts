export declare class ConnectionOptions {
    connectionName: string;
    host: string;
    port: number;
    username: string;
    dbName: string;
    password: string;
    type: "postgres";
}
