export declare class AzureQueueConnectionOptions {
    queueName: string;
    accountName: string;
    accountKey: string;
    queueUrl: string;
}
