import { ConnectionOptions } from "typeorm";
export declare class ConnectionManagerOptions {
    connectionOptions: ConnectionOptions;
    connectionName: string;
}
