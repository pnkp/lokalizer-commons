import { Connection } from "typeorm";
export declare class ConnectionCollection {
    connection: Connection;
    connectionName: string;
}
