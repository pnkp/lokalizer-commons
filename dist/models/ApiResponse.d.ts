export declare class ApiResponse<T> {
    data: T;
    status: number;
    statusText: string;
    headers: any;
    request?: any;
}
