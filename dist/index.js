'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Axios = _interopDefault(require('axios'));
var inversify = require('inversify');
require('reflect-metadata');
var storageQueue = require('@azure/storage-queue');
var typeorm = require('typeorm');
var lodash = require('lodash');
var uuid = require('uuid');

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

exports.ApiRequest = class ApiRequest {
    send(request) {
        return __awaiter(this, void 0, void 0, function* () {
            return Axios.request(request).then((response) => {
                const apiResponse = response;
                return apiResponse;
            });
        });
    }
};
exports.ApiRequest = __decorate([
    inversify.injectable()
], exports.ApiRequest);

exports.AbstractAzureQueueClient = class AbstractAzureQueueClient {
    getClient() {
        const sharedKeyCredential = new storageQueue.SharedKeyCredential(this.connectionOptions.accountName, this.connectionOptions.accountKey);
        const tokenCredential = new storageQueue.TokenCredential("token");
        tokenCredential.token = "renewedToken"; // Renew the token by updating token field of token credential
        const pipeline = storageQueue.StorageURL.newPipeline(sharedKeyCredential, {
            retryOptions: { maxTries: 4 },
            telemetry: { value: "BasicSample V10.0.0" },
        });
        const serviceURL = new storageQueue.ServiceURL(this.connectionOptions.queueUrl, pipeline);
        return storageQueue.MessagesURL.fromQueueURL(storageQueue.QueueURL.fromServiceURL(serviceURL, this.connectionOptions.queueName));
    }
};
exports.AbstractAzureQueueClient = __decorate([
    inversify.injectable()
], exports.AbstractAzureQueueClient);

class AzureQueueConnectionOptions {
}

exports.LokalizerQueueService = class LokalizerQueueService extends exports.AbstractAzureQueueClient {
    constructor(options) {
        super();
        this.connectionOptions = options;
    }
    sendMessage(message) {
        return __awaiter(this, void 0, void 0, function* () {
            const enqueueQueueResponse = yield this.getClient().enqueue(storageQueue.Aborter.none, Buffer.from(JSON.stringify(message)).toString("base64"));
            console.log(`Enqueue message successfully, service assigned message Id: ${enqueueQueueResponse.messageId}, service assigned request Id: ${enqueueQueueResponse.requestId}`);
            return enqueueQueueResponse;
        });
    }
};
exports.LokalizerQueueService = __decorate([
    inversify.injectable(),
    __metadata("design:paramtypes", [AzureQueueConnectionOptions])
], exports.LokalizerQueueService);

exports.ConnectionManager = class ConnectionManager {
    constructor() {
        this.connections = [];
    }
    createConnectionAsync(connectionOptions) {
        return __awaiter(this, void 0, void 0, function* () {
            const existingConnection = yield this.getConnectionAsync(connectionOptions.connectionName);
            if (!lodash.isNil(existingConnection)) {
                return existingConnection;
            }
            const connection = yield typeorm.createConnection(connectionOptions.connectionOptions);
            this.connections.push({
                connectionName: connectionOptions.connectionName,
                connection,
            });
            return connection;
        });
    }
    getConnectionAsync(connectionName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (lodash.isNil(connectionName)) {
                connectionName = "default";
            }
            // @ts-ignore
            const connectionCollection = this.connections.find((connection) => connection.connectionName === connectionName);
            if (!lodash.isNil(connectionCollection)) {
                return connectionCollection.connection;
            }
            return null;
        });
    }
};
exports.ConnectionManager = __decorate([
    inversify.injectable()
], exports.ConnectionManager);

exports.ConnectionManagerForAzureFunctions = class ConnectionManagerForAzureFunctions {
    constructor(options) {
        this._options = options;
    }
    createConnectionAsync(connectionOptions) {
        return __awaiter(this, void 0, void 0, function* () {
            return typeorm.getConnection();
        });
    }
    getConnectionAsync(connectionName) {
        return __awaiter(this, void 0, void 0, function* () {
            let connection;
            try {
                connection = typeorm.getConnection(connectionName);
                this.injectConnectionOptions(connection, this._options);
                if (!connection.isConnected) {
                    yield connection.connect();
                }
                return connection;
            }
            catch (error) {
                console.error(`${JSON.stringify(error)}`);
            }
            const notCollidingOptions = Object.assign({ name: uuid.v4() }, this._options);
            return yield typeorm.createConnection(notCollidingOptions);
        });
    }
    injectConnectionOptions(connection, connectionOptions) {
        /**
         * from Connection constructor()
         */
        // @ts-ignore
        connection.options = connectionOptions;
        // @ts-ignore
        connection.manager = connection.createEntityManager();
        /**
         * from Connection connect()
         */
        // @ts-ignore
        connection.buildMetadatas();
        return connection;
    }
};
exports.ConnectionManagerForAzureFunctions = __decorate([
    inversify.injectable(),
    __metadata("design:paramtypes", [Object])
], exports.ConnectionManagerForAzureFunctions);

exports.LoggerService = class LoggerService {
    error(message) {
        console.log(message);
    }
    log(message) {
        console.log(message);
    }
};
exports.LoggerService = __decorate([
    inversify.injectable()
], exports.LoggerService);

class ApiResponse {
}

class ConnectionCollection {
}

class ConnectionManagerOptions {
}

class ConnectionOptions {
}

class RequestOptions {
}
(function (RequestMethodEnum) {
    RequestMethodEnum["GET"] = "GET";
    RequestMethodEnum["POST"] = "POST";
})(exports.RequestMethodEnum || (exports.RequestMethodEnum = {}));

exports.BaseRepository = class BaseRepository {
    getRepositoryAsync(connectionName = "default") {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = yield this._connectionManager.getConnectionAsync(connectionName);
            return connection.getRepository(this.entity.name);
        });
    }
};
exports.BaseRepository = __decorate([
    inversify.injectable()
], exports.BaseRepository);

exports.ApiResponse = ApiResponse;
exports.AzureQueueConnectionOptions = AzureQueueConnectionOptions;
exports.ConnectionCollection = ConnectionCollection;
exports.ConnectionManagerOptions = ConnectionManagerOptions;
exports.ConnectionOptions = ConnectionOptions;
exports.RequestOptions = RequestOptions;
