import { ConnectionManagerOptions, IConnectionManager } from "..";
import { Connection, Repository } from "typeorm";
export declare abstract class BaseRepository<TEntity> {
    private _connectionManager;
    protected abstract entity: new () => TEntity;
    protected constructor(_connectionManager: IConnectionManager<ConnectionManagerOptions, Connection>);
    getRepositoryAsync(connectionName?: string): Promise<Repository<TEntity>>;
}
