import "reflect-metadata";
import { IApiRequest } from "./IApiRequest";
import { ApiResponse, RequestOptions } from "..";
export declare class ApiRequest implements IApiRequest {
    send<T>(request: RequestOptions): Promise<ApiResponse<T>>;
}
