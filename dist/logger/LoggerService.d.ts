import "reflect-metadata";
import { ILoggerService } from "./ILoggerService";
export declare class LoggerService implements ILoggerService<any> {
    error(message: any): void;
    log(message: any): void;
}
