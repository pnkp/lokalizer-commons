import { IConnectionManager } from "..";
import { Connection, Repository } from "typeorm";
export declare abstract class BaseRepository<TEntity, TConnectionParam> {
    protected abstract entity: new () => TEntity;
    protected abstract _connectionManager: IConnectionManager<TConnectionParam, Connection>;
    getRepositoryAsync(connectionName?: string): Promise<Repository<TEntity>>;
}
