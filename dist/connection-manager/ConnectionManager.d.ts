import { Connection } from "typeorm";
import { IConnectionManager } from "./IConnectionManager";
import { ConnectionCollection, ConnectionManagerOptions } from "..";
export declare class ConnectionManager implements IConnectionManager<ConnectionManagerOptions, Connection | null> {
    connections: ConnectionCollection[];
    createConnectionAsync(connectionOptions: ConnectionManagerOptions): Promise<Connection>;
    getConnectionAsync(connectionName?: string): Promise<Connection | null>;
}
