import "reflect-metadata";
import { IConnectionManager } from "./IConnectionManager";
import { Connection, ConnectionOptions } from "typeorm";
export declare class ConnectionManagerForAzureFunctions implements IConnectionManager<ConnectionOptions, Connection> {
    private readonly _options;
    constructor(options: ConnectionOptions);
    createConnectionAsync(connectionOptions: ConnectionOptions): Promise<Connection>;
    getConnectionAsync(connectionName?: string): Promise<Connection>;
    injectConnectionOptions(connection: Connection, connectionOptions: ConnectionOptions): Connection;
}
