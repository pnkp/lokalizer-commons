import "reflect-metadata";
import { ILokalizerQueueService } from "./ILokalizerQueueService";
import { MessagesEnqueueResponse } from "@azure/storage-queue";
import { AbstractAzureQueueClient } from "./AbstractAzureQueueClient";
import { AzureQueueConnectionOptions } from "../models/AzureQueueConnectionOptions";
export declare class LokalizerQueueService<TData> extends AbstractAzureQueueClient implements ILokalizerQueueService<TData> {
    protected connectionOptions: AzureQueueConnectionOptions;
    constructor(options: AzureQueueConnectionOptions);
    sendMessage(message: TData): Promise<MessagesEnqueueResponse>;
}
