import { MessagesEnqueueResponse } from "@azure/storage-queue";
export interface ILokalizerQueueService<TData> {
    sendMessage(message: TData): Promise<MessagesEnqueueResponse>;
}
