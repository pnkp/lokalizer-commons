import { MessagesURL } from "@azure/storage-queue";
import { AzureQueueConnectionOptions } from "../models/AzureQueueConnectionOptions";
export declare abstract class AbstractAzureQueueClient {
    protected abstract connectionOptions: AzureQueueConnectionOptions;
    protected getClient(): MessagesURL;
}
