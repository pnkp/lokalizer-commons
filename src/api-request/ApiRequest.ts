import Axios from "axios";
import {injectable} from "inversify";
import "reflect-metadata";
import {IApiRequest} from "./IApiRequest";
import {ApiResponse, RequestOptions} from "..";

@injectable()
export class ApiRequest implements IApiRequest {

    public async send<T>(request: RequestOptions): Promise<ApiResponse<T>> {
        return Axios.request(request).then((response) => {
            const apiResponse: ApiResponse<T> = response;
            return apiResponse;
        });
    }
}
