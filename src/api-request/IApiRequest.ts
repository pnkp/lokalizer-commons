import {ApiResponse, RequestOptions} from "..";

export interface IApiRequest {
    send<T>(requestOptions: RequestOptions): Promise<ApiResponse<T>>;
}
