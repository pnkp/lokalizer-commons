export interface IConnectionManager<TParam, TConnection> {
    createConnectionAsync(connectionOptions: TParam): Promise<TConnection>;
    getConnectionAsync(connectionName?: string): Promise<TConnection>;
}
