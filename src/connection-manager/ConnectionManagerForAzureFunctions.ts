import {injectable} from "inversify";
import "reflect-metadata";
import {IConnectionManager} from "./IConnectionManager";
import {Connection, ConnectionOptions, createConnection, getConnection} from "typeorm";
import { v4 as uuid } from "uuid";

@injectable()
export class ConnectionManagerForAzureFunctions implements IConnectionManager<ConnectionOptions, Connection> {
    private readonly _options: ConnectionOptions;

    public constructor(options: ConnectionOptions) {
        this._options = options;
    }

    public async createConnectionAsync(connectionOptions: ConnectionOptions): Promise<Connection> {
        return getConnection();
    }

    public async getConnectionAsync(connectionName?: string): Promise<Connection> {
        let connection: Connection;

        try {
            connection = getConnection(connectionName);
            this.injectConnectionOptions(connection, this._options);
            if (!connection.isConnected) {
                await connection.connect();
            }
            return connection;
        } catch (error) {
            console.error(`${JSON.stringify(error)}`);
        }

        const notCollidingOptions: ConnectionOptions = Object.assign({name: uuid()}, this._options);
        return await createConnection(notCollidingOptions);
    }

    public injectConnectionOptions(connection: Connection, connectionOptions: ConnectionOptions): Connection {
        /**
         * from Connection constructor()
         */

        // @ts-ignore
        connection.options = connectionOptions;
        // @ts-ignore
        connection.manager = connection.createEntityManager();

        /**
         * from Connection connect()
         */
        // @ts-ignore
        connection.buildMetadatas();

        return connection;
    }
}

