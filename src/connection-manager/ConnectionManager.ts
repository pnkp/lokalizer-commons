import {Connection, createConnection} from "typeorm";
import {IConnectionManager} from "./IConnectionManager";
import {injectable} from "inversify";
import {isNil} from "lodash";
import {ConnectionCollection, ConnectionManagerOptions} from "..";

@injectable()
export class ConnectionManager implements IConnectionManager<ConnectionManagerOptions, Connection | null> {
    public connections: ConnectionCollection[] = [];

    public async createConnectionAsync(connectionOptions: ConnectionManagerOptions): Promise<Connection> {
        const existingConnection: Connection|null
            = await this.getConnectionAsync(connectionOptions.connectionName);
        if (!isNil(existingConnection)) {
            return existingConnection;
        }

        const connection: Connection = await createConnection(connectionOptions.connectionOptions);
        this.connections.push({
            connectionName: connectionOptions.connectionName,
            connection,
        });
        return connection;
    }

    public async getConnectionAsync(connectionName?: string): Promise<Connection | null> {
        if (isNil(connectionName)) {
            connectionName = "default";
        }

        // @ts-ignore
        const connectionCollection: ConnectionCollection =
            this.connections.find((connection) => connection.connectionName === connectionName);
        if (!isNil(connectionCollection)) {
            return connectionCollection.connection;
        }

        return null;
    }
}
