import {injectable} from "inversify";
import {IConnectionManager} from "..";
import {Connection, Repository} from "typeorm";

@injectable()
export abstract class BaseRepository<TEntity, TConnectionParam> {
    protected abstract entity: new () => TEntity;

    protected abstract _connectionManager: IConnectionManager<TConnectionParam, Connection>;

    public async getRepositoryAsync(connectionName: string = "default"): Promise<Repository<TEntity>> {
        const connection: Connection = await this._connectionManager.getConnectionAsync(connectionName);
        return connection.getRepository<TEntity>(this.entity.name);
    }
}
