export interface ILoggerService<TParam> {
    log(message: TParam): void;
    error(message: TParam): void;
}
