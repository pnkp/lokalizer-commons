import "reflect-metadata";
import {ILoggerService} from "./ILoggerService";
import {injectable} from "inversify";

@injectable()
export class LoggerService implements ILoggerService<any> {
    public error(message: any): void {
        console.log(message);
    }

    public log(message: any): void {
        console.log(message);
    }
}
