export class ApiResponse<T> {
    public data: T;
    public status: number;
    public statusText: string;
    public headers: any;
    public request?: any;
}
