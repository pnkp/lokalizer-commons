export class AzureQueueConnectionOptions {
    public queueName: string;
    public accountName: string;
    public accountKey: string;
    public queueUrl: string;
}
