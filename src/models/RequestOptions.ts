export class RequestOptions {
    public headers?: object;
    public params?: object;
    public data?: object;
    public timeout?: number;
    public url: string;
    public method: RequestMethodEnum;
}

export enum RequestMethodEnum {
    GET = "GET",
    POST = "POST",
}
