import {Connection} from "typeorm";

export class ConnectionCollection {
    public connection: Connection;
    public connectionName: string;
}
