export class ConnectionOptions {
    public connectionName: string;
    public host: string;
    public port: number;
    public username: string;
    public dbName: string;
    public password: string;
    public type: "postgres";
}
