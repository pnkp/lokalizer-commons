import {ConnectionOptions} from "typeorm";

export class ConnectionManagerOptions {
    public connectionOptions: ConnectionOptions;
    public connectionName: string;
}
