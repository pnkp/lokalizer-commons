import {
    MessagesURL,
    Pipeline,
    QueueURL,
    ServiceURL,
    SharedKeyCredential,
    StorageURL,
    TokenCredential,
} from "@azure/storage-queue";
import {injectable} from "inversify";
import {AzureQueueConnectionOptions} from "../models/AzureQueueConnectionOptions";

@injectable()
export abstract class AbstractAzureQueueClient {

    protected abstract connectionOptions: AzureQueueConnectionOptions;

    protected getClient(): MessagesURL {
        const sharedKeyCredential: SharedKeyCredential =
            new SharedKeyCredential(this.connectionOptions.accountName, this.connectionOptions.accountKey);

        const tokenCredential: TokenCredential = new TokenCredential("token");
        tokenCredential.token = "renewedToken"; // Renew the token by updating token field of token credential

        const pipeline: Pipeline = StorageURL.newPipeline(sharedKeyCredential, {
            retryOptions: {maxTries: 4},
            telemetry: {value: "BasicSample V10.0.0"},
        });

        const serviceURL: ServiceURL = new ServiceURL(
            this.connectionOptions.queueUrl,
            pipeline,
        );

        return MessagesURL.fromQueueURL(QueueURL.fromServiceURL(serviceURL, this.connectionOptions.queueName));
    }
}
