import {injectable} from "inversify";
import "reflect-metadata";
import {ILokalizerQueueService} from "./ILokalizerQueueService";
import {Aborter, MessagesEnqueueResponse} from "@azure/storage-queue";
import {AbstractAzureQueueClient} from "./AbstractAzureQueueClient";
import {AzureQueueConnectionOptions} from "../models/AzureQueueConnectionOptions";

@injectable()
export class LokalizerQueueService<TData> extends AbstractAzureQueueClient implements ILokalizerQueueService<TData> {

    protected connectionOptions: AzureQueueConnectionOptions;

    constructor(options: AzureQueueConnectionOptions) {
        super();
        this.connectionOptions = options;
    }

    public async sendMessage(message: TData): Promise<MessagesEnqueueResponse> {
        const enqueueQueueResponse: MessagesEnqueueResponse = await this.getClient().enqueue(
            Aborter.none,
            Buffer.from(JSON.stringify(message)).toString("base64"),
        );
        console.log(
            `Enqueue message successfully, service assigned message Id: ${
                enqueueQueueResponse.messageId
            }, service assigned request Id: ${enqueueQueueResponse.requestId}`,
        );

        return enqueueQueueResponse;
    }
}
